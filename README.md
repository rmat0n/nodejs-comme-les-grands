# Node.js comme les grands

## Pitch

Développer et mettre en production une application Node.js, ça vous fait :scream: ? Vous pensez que le langage n'est pas terrible ? Vous trouvez qu'il n'est pas assez outillé ? Marre de chercher les bonnes pratiques associées ?

Stoppez tout et "venez chercher bonheur" dans cette session retour d'xp sur Node.js !
Les grands i.e. les géants du web y arrivent, nous pouvons nous inspirer de toutes leurs bonnes pratiques pour, nous aussi, faire plus facilement du Node.js en entreprise :metal:

Nous verrons les nouveautés des dernières versions, les débats et pratiques de code actuels, les outils pour nous aider en production (bugs, crashs...), quelques petits puzzlers pour égayer cet exigeant public et bien d'autres !

## Bio

Consultant indépendant, Romain Maton s’est forgé une solide expérience en développement Web et logiciel depuis 2005. Il dispose ainsi d’une très large compétence sur l’ensemble de l’écosystème Java/JEE et Node.js, que ce soit sur les bonnes pratiques d’architecture, sur les frameworks de développement ou sur les applications single page.

Il est intervenu dans plusieurs conférences et JUG de France sur des sujets tels que Scala, Node.js, l’optimisation de sites Web mobiles, live coding Marionette.js ou la-quête-du-graal.js.

Créateur de web-tambouille.fr, où vous pourrez lire des articles sur des sujets tels que Java, Node.js, Javascript, Programmation Fonctionnelle, Android et bien d’autres !

## Conférences

* [BDX.IO 2017](https://www.bdx.io/#/program)
* [Soft Shake 2017](https://www.kora.li/admin.html#/index/p?u=rmat0n&s=node-js-comme-les-grands&c=softshake&e=2017)
* [Devoxx France 2017](http://cfp.devoxx.fr/2017/talk/ZCK-5980/Node.js_comme_les_grands)
